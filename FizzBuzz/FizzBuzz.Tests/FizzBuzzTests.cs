﻿namespace FizzBuzz.Tests
{
    using System;
    using FizzBuzz.Services.Factories.Impl;
    using Xunit;
    using Assert = Xunit.Assert;

    public class FizzBuzzTests
    {
        [Theory]
        [InlineData(3)]
        [InlineData(15)]
        public void IsFizz_WhenProvidedNumberIsDivisibleByThree_ReturnsCorrectString(int number)
        {
            var fizzString = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "wizz" : "fizz";
            var fizzNumberFactory = new FizzNumberFactory();

            var isFizz = fizzNumberFactory.IsFizz(number);

            Assert.Equal(fizzString, isFizz);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(1)]
        public void IsFizz_WhenProvidedNumberIsNotDivisibleByThree_ReturnsEmptyString(int number)
        {
            var fizzNumberFactory = new FizzNumberFactory();

            var isFizz = fizzNumberFactory.IsFizz(number);

            Assert.Equal(String.Empty, isFizz);
        }

        [Theory]
        [InlineData(15)]
        [InlineData(25)]
        public void IsBuzz_WhenProvidedNumberIsDivisibleByFive_ReturnsCorrectString(int number)
        {
            var buzzString = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz";
            var buzzNumberFactory = new BuzzNumberFactory();

            var isBuzz = buzzNumberFactory.IsBuzz(number);

            Assert.Equal(buzzString, isBuzz);
        }

        [Theory]
        [InlineData(9)]
        [InlineData(13)]
        public void IsBuzz_WhenProvidedNumberIsNotDivisibleByFive_ReturnsEmptyString(int number)
        {
            var buzzNumberFactory = new BuzzNumberFactory();

            var isBuzz = buzzNumberFactory.IsBuzz(number);

            Assert.Equal(String.Empty, isBuzz);
        }
    }
}

﻿namespace FizzBuzz.Models.ViewModels
{
    public class FizzBuzzNumberViewModel
    {
        public int Number { get; set; }

        public string IsFizz { get; set; }

        public string IsBuzz { get; set; }
    }
}
﻿namespace FizzBuzz.Models
{
    using System.ComponentModel.DataAnnotations;

    public class FizzBuzzNumberRequest
    {

        [Range(1, 1000, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Number { get; set; }

        public int? Page { get; set; }
    }
}
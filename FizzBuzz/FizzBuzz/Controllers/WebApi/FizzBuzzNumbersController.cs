﻿namespace FizzBuzz.Controllers.WebApi
{
    using System.Web.Http;
    using FizzBuzz.Models.ViewModels;
    using System.Collections.Generic;
    using Ninject.Infrastructure.Language;

    public class FizzBuzzNumbersController : ApiController
    {
        // GET api/FizzBuzzNumbers/
        /// <summary>
        /// Gets the list of all numbers
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FizzBuzzNumberViewModel> Get()
        {
            var data = new List<FizzBuzzNumberViewModel>().ToEnumerable();

            return data;
        }

        // GET api/FizzBuzzNumbers/3
        /// <summary>
        /// Gets single number data numbers
        /// </summary>
        /// <returns></returns>
        public FizzBuzzNumberViewModel Get(int number)
        {
            return new FizzBuzzNumberViewModel();
        }

        // POST api/FizzBuzzNumbers/
        /// <summary>
        /// Sets new FizzBuzzNumbers
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Post([FromBody] string value)
        {
            return Ok();
        }

        // PUT api/FizzBuzzNombers/3
        /// <summary>
        /// Modifies single number data numbers
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Put(int number, [FromBody] string value)
        {
            return Ok();
        }

        // PUT api/FizzBuzzNombers/3
        /// <summary>
        /// deletes single number
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Delete(int number)
        {
            return Ok();
        }
    }
}

﻿namespace FizzBuzz.Controllers
{
    using System.Web.Mvc;
    using FizzBuzz.Models;
    using FizzBuzz.Services.Services;
    using PagedList;
    using System.Collections.Generic;
    using FizzBuzz.Models.ViewModels;

    public class HomeController : Controller
    {
        private readonly IFizzBuzzNumbersService _fizzBuzzNumbersService;

        public HomeController(IFizzBuzzNumbersService fizzBuzzNumbersService)
        {
            _fizzBuzzNumbersService = fizzBuzzNumbersService;
        }

        // GET: Home
        public ActionResult Index(FizzBuzzNumberRequest request)
        {
            var pageSize = 20;
            var pageNumber = request.Page ?? 1;

            if (!TryValidateModel(request))
            {
                return View(new List<FizzBuzzNumberViewModel>().ToPagedList(pageNumber, pageSize));
            }

            ViewBag.Number = request.Number;

            var data = _fizzBuzzNumbersService.GetFizzBuzzNumbers(request.Number);

            return View(data.ToPagedList(pageNumber, pageSize));
        }
    }
}
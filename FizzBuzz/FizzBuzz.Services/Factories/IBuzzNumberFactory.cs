﻿namespace FizzBuzz.Services.Factories
{
    public interface IBuzzNumberFactory
    {
        string IsBuzz(int number);
    }
}

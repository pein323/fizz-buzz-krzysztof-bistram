﻿namespace FizzBuzz.Services.Factories
{
    public interface IFizzNumberFactory
    {
        string IsFizz(int number);
    }
}
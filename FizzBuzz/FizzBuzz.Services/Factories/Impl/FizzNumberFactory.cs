﻿namespace FizzBuzz.Services.Factories.Impl
{
    using System;

    public class FizzNumberFactory : IFizzNumberFactory
    {
        private const string Fizz = "fizz";
        private const string Wizz = "wizz";

        public string IsFizz(int number)
        {
            if (CheckIfFizz(number))
            {
                return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? Wizz : Fizz;
            }

            return String.Empty;
        }

        private bool CheckIfFizz(int number)
        {
            return number % 3 == 0;
        }
    }
}
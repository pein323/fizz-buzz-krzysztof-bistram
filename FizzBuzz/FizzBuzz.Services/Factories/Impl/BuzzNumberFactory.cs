﻿namespace FizzBuzz.Services.Factories.Impl
{
    using System;
    public class BuzzNumberFactory : IBuzzNumberFactory
    {
        private const string Buzz = "buzz";
        private const string Wuzz = "wuzz";

        public string IsBuzz(int number)
        {
            if (CheckBuzz(number))
            {
                return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? Wuzz : Buzz;
            }

            return String.Empty;
        }
        private bool CheckBuzz(int number)
        {
            return number % 5 == 0;
        }
    }
}
﻿namespace FizzBuzz.Services.Services.Impl
{
    using System.Collections.Generic;
    using FizzBuzz.Models.ViewModels;
    using FizzBuzz.Services.Factories;

    public class FizzBuzzNumbersService : IFizzBuzzNumbersService
    {
        private readonly IFizzNumberFactory _fizzNumberFactory;
        private readonly IBuzzNumberFactory _buzzNumberFactory;

        public FizzBuzzNumbersService(
            IFizzNumberFactory fizzNumberFactory,
            IBuzzNumberFactory buzzNumberFactory)
        {
            _fizzNumberFactory = fizzNumberFactory;
            _buzzNumberFactory = buzzNumberFactory;
        }
        public List<FizzBuzzNumberViewModel> GetFizzBuzzNumbers(int number)
        {
            var fizzyBuzzNumbers = new List<FizzBuzzNumberViewModel>();

            for (int currentNumber = 1; currentNumber <= number; currentNumber++)
            {
                fizzyBuzzNumbers.Add(new FizzBuzzNumberViewModel
                {
                    Number = currentNumber,
                    IsBuzz = _buzzNumberFactory.IsBuzz(currentNumber),
                    IsFizz = _fizzNumberFactory.IsFizz(currentNumber)
                });
            }

            return fizzyBuzzNumbers;
        }
    }
}
﻿namespace FizzBuzz.Services.Services
{
    using System.Collections.Generic;
    using FizzBuzz.Models.ViewModels;

    public interface IFizzBuzzNumbersService
    {
        List<FizzBuzzNumberViewModel> GetFizzBuzzNumbers(int number);
    }
}